import React from "react";
import {Tabs, Tab, Jumbotron, Container} from "react-bootstrap";
import Todo from "../Todo/App";
import Jobs from "../Jobs/App";


const App = () => {
  return (
    <Container>
        <Jumbotron>
            <h1>Test App</h1>
        </Jumbotron>

        <Tabs defaultActiveKey={1} id="tabs">
          <Tab eventKey={1} title="Jobs">
            <Jobs />
          </Tab>
          <Tab eventKey={2} title="Todo">
            <Todo />
          </Tab>
        </Tabs>

      </Container>
  );

}

export default App;
