import React,{useState} from "react";
import {ToggleButtonGroup,ToggleButton,Form, Card} from "react-bootstrap";
import JobDescription from "./JobDescription";

const App = () =>{
  const [jobs, setjobs] = useState([]);

  const onChange  = (value)=>{
    let i = 0, jobs = [];
    while (++i <= value) jobs.push(i);
    setjobs(jobs);
  }

  return (
      <Card>
        <Card.Body>
        <ToggleButtonGroup ntype="radio" name="options" defaultValue={0} onChange={onChange}>
          <ToggleButton value={0}>0</ToggleButton>
          <ToggleButton value={1}>1</ToggleButton>
          <ToggleButton value={2}>2</ToggleButton>
          <ToggleButton value={3}>4</ToggleButton>
        </ToggleButtonGroup>
       
        <div>
          {
            jobs.map((item,i)=> <JobDescription jobId={i+1} key={i}/>)
          }

          {jobs.length===0 &&
          <div> 
            <Form.Group controlId="exampleForm.ControlInput1">
                <Form.Label>What is your current status?</Form.Label>
                <Form.Control as="select">
                <option>1</option>
                <option>2</option>
                <option>3</option>
                <option>4</option>
                <option>5</option>
                </Form.Control>
            </Form.Group>
            </div>}
        </div>
        </Card.Body>
      </Card>
    )
}



export default App;
